# ------------------------------------------------------------ #
# Ubiqum Module 2, Sprint 3
# Customer Market Basket Analysis
# File: apriori_brand_category
# Author: Danielle Jeffery
# Date: February 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(arules)
library(arulesViz)

# reads transactions from csv
transactions_brand_category <- read.transactions("data/transactions_by_brand_category.csv", 
                                                 format = "single", 
                                                 header = TRUE, 
                                                 sep = ",",
                                                 cols = c("id_order", "brand_category"))

# applies apriori algorithm and generates rules
rules_brand_category <- apriori(transactions_brand_category, 
                                parameter = list(supp = 0.001, 
                                                 conf = 0.3))

# displays rules and metrics
summary(rules_brand_category)

inspect(rules_brand_category)

# graphs rules
plot(rules_brand_category, 
     method = "graph", 
     measure = "count", 
     shading = "lift",
     engine = "htmlwidget")

itemFrequencyPlot(transactions_brand_category, top = 10)

image(transactions_brand_category[1:3000])
