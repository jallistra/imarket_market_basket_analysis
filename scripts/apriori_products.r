# ------------------------------------------------------------ #
# Ubiqum Module 2, Sprint 3
# Customer Market Basket Analysis
# File: apriori_products
# Author: Danielle Jeffery
# Date: February 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(arules)
library(arulesViz)

# reads transactions from csv
transactions_product <- read.transactions("data/transactions_by_product.csv", 
                                          format = "single", 
                                          header = TRUE, 
                                          sep = ",",
                                          cols = c("id_order", "sku"))

# applies apriori algorithm and generates rules
rules_product <- apriori(transactions_product, 
                         parameter = list(supp = 0.0006, 
                                          conf = 0.3))

# displays rules and metrics
summary(rules_product)

inspect(rules_product)

# graphs rules
plot(rules_product, 
     method = "graph", 
     measure = "count", 
     shading = "lift",
     engine = "htmlwidget")

itemFrequencyPlot(transactions_product, top = 10)

image(transactions_product[1:3000])

