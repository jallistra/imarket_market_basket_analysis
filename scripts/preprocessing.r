# ------------------------------------------------------------ #
# Ubiqum Module 2, Sprint 3
# Customer Market Basket Analysis
# File: preprocessing
# Author: Danielle Jeffery
# Date: February 2020
# Version: 0.1
# ------------------------------------------------------------ #

# loads libraries ####
library(dplyr)
library(ggplot2)

# reads data ####
line_item <- read.csv("data/line_item.csv")
orders <- read.csv("data/orders.csv")
products <- read.csv("data/products.csv")

# data cleaning ####

# converts factors to chars or numbers

line_item$sku <- as.character(line_item$sku)

line_item$unit_price <- as.numeric(as.character(sub(",", ".", line_item$unit_price)))

products$sku <- as.character(products$sku)

products$name_en <- as.character(products$name_en)

orders$state <- as.character(orders$state)

orders$total_paid <- as.numeric(as.character(sub(",", ".", orders$total_paid)))

# removes all incomplete orders from orders table
orders <- orders %>% 
  filter(state == 'Completed')

# inner joins all tables to compare price differences
price_summary <- orders %>% 
                  inner_join(line_item, by = "id_order") %>% 
                  inner_join(products, by = "sku") %>% 
                  select(id_order, product_quantity, price, unit_price, total_paid) %>% 
                  group_by(id_order) %>% 
                  summarise(tot_price = sum(price * product_quantity), 
                              tot_unit_price = sum(unit_price * product_quantity), 
                              mean_paid = mean(total_paid)) %>% arrange(id_order)

# plots line item totals against total paid on orders
price_summary %>%
  ggplot(aes(tot_unit_price, mean_paid)) +
  geom_point() +
  theme_minimal()

# removes entries where the total paid on order is more than 5% greater than the total line unit price
filtered_price_summary <- price_summary %>% 
                            filter((mean_paid - tot_unit_price) < (mean_paid / 20))

# plots line item totals against total paid on orders after filtering
filtered_price_summary %>%
  ggplot(aes(tot_unit_price, mean_paid)) +
  geom_point() +
  theme_minimal()

# creates line_item df that includes only those included in filtered price summary
line_item_cleaned <- line_item %>% 
                       semi_join(filtered_price_summary, by = "id_order")

# creates transactions df with id_order and sku
transactions_by_product <- line_item_cleaned %>% 
                             select(id_order, sku)

# creates transactions df with id_order and brand_category
transactions_by_brand_category <- line_item_cleaned %>% 
                                    inner_join(products, by = "sku") %>% 
                                    filter(!is.na(brand)) %>% 
                                    mutate(brand_category = paste(brand, "-", manual_categories)) %>% 
                                    select(id_order, brand_category) 
                                    
# creates csv for each grouping (by product, category, and brands)
write.csv(transactions_by_product,
          "data/transactions_by_product.csv", 
          row.names = FALSE)

write.csv(transactions_by_brand_category,
          "data/transactions_by_brand_category.csv", 
          row.names = FALSE)
